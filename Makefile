#!/bin/bash
all:
	echo *Notice: Given that python can handle compressed files internally, with [-f] you can specify the compressed file to open 

	python3 tarea.py -s 3 -o 1 -bp 0 -gh 4 -ph 3 -f branch-trace-gcc.trace.gz
	python3 tarea.py -s 4 -o 1 -bp 1 -gh 3 -ph 3 -f branch-trace-gcc.trace.gz
	python3 tarea.py -s 5 -o 1 -bp 2 -gh 3 -ph 2 -f branch-trace-gcc.trace.gz
	python3 tarea.py -s 3 -o 1 -bp 3 -gh 4 -ph 3 -f branch-trace-gcc.trace.gz
	