import numpy as np
import array
import gzip
import sys, getopt
import time 

'''
    Universidad de Costa Rica 
    Escuela de Ingenieria Electrica
    IE0521: Estructuras de Computadoras Digitales II
    Tarea 1: Predictores de saltos

        Emmanuel Rivel Montero, B65868
    
    -------------------------------------------------------
    This .py contains 5 principal functions: one for each predictor required and one main function.
    The main function handles the options passed by the user and calls the respective branch predictor: 
        -bimodal
        -gshared
        -pshared
        -tournament
    -------------------------------------------------------

    NOTES ¡IMPORTANT!: 
    *** -Given that python can handle internally compressed files without de-compressing them, in this program the user can either use the option [-f] in console to specify the compressed file to handle OR it's default value is the name of the compressed file given in this assignment.

        - In the entire file it's defined by the user that:
            For the bp value:
            - bp = 0: bimodal
            - bp = 1: gshared
            - bp = 2: pshared
            - bp = 3: tournament

            For the states:
            - 0: Strongly not taken
            - 1: Weakly not taken
            - 2: Weakly taken
            - 3: Strongly taken

'''
starting_time = time.time() #Global variable to save the starting time of the program

def bimodal(s, o, gh, ph, file_name):
    '''
        Bimodal brach predictor: Uses an array of counters wich can hadle 4 possible states, therefore the counters are made of 2-bits each.
        -Inputs:
            -s: BTH table size. In this function is used for sizing the counter array.
            -o: Debug flag wich prints locally the results and stops the iterations at 5k.
            -gh: Gshare reg size. In this function is used to print the value given by the user at the end.
            -ph: Pshare reg siz. In this function is used to print the value given by the user at the end.
            -file_name: name of the compressed file to be read in the function.

        -Outputs:
            -mode: String with the branch predictors name
            -cont: Counter with the number of line of the compressed file reads.
            -hits_T: Counter for the correct T branches
            -misses_T: Counter for the incorrect T branches
            -hits_N: Counter for the incorrect N branches
            -misses_N: Size of the array of counters

    '''

    mode = 'Bimodal'
    name = 'Bimodal.txt'    #Name used to create the file if o = 1
    hits_T = 0              #Counter for the correct T branches
    misses_T = 0            #Counter for the incorrect T branches
    hits_N = 0              #Counter for the correct N branches
    misses_N = 0            #Counter for the incorrect N branches
    counters_size = 2**s    #Size of the array of counters
    counters = []           #Array of counters

    #Initializing array of counters in strongly not taken
    for element in range(counters_size):
        counters.append(0)


    mask = 2**s - 1 #Creating a mask for the size of the direction
    cont = 0        #Counter for the lines in the file

    if o == 1:                  #Debug flag
        fo= open(name, "w+")    #Creates the file in write mode
        fo.write("    PC\t\tOutcome\t  Prediction\tCorrect/Incorrect\n")   #prints in file 

    with gzip.open(file_name, "r") as f:
        for line in f:
            #Parsing the string to a desired value
            string = line[:12].decode('utf-8')      #Taking the last 12 elements of the line and removing it's "0b" initializer 
            string = string.strip('\n')             #Removing new line seq of the string
            splitted = string.split(" ")            #Spliting string by spaces
            dec_dir = splitted[0]   #PC (decimal_direction)
            dec_dir = int(dec_dir)  #Parsing from str to int the PC
            result = splitted[1]    #Result of the brach (T/N)
            #Apliying the mask (&) to the PC
            counter_to_acess = dec_dir & mask
            # Definition of the possible cases:
            if counters[counter_to_acess] == 0 or counters[counter_to_acess] == 1:
                #This case is for N prediction
                if result == 'N':   #result = N -> hit
                    hits_N += 1
                    if counters[counter_to_acess] > 0:      #If previous value  in the counter was WN [1]
                        counters[counter_to_acess] -= 1     #Turn to SN [0]
                    else:
                        counters[counter_to_acess] += 1     #If it was SN [0]
                    if o == 1:
                        fo.write(f"{dec_dir}\t   {result}\t      N\t\t    correct\n")
                else:       #result = T -> miss
                    misses_N += 1
                    if o == 1:
                        fo.write(f"{dec_dir}\t   {result}\t      N\t\t    incorrect\n")
                    counters[counter_to_acess] += 1         #Move either from SN to WN or from WN to WT
            elif counters[counter_to_acess] == 2 or counters[counter_to_acess] == 3:
                #This case is for T prediction
                if result == 'T':   #result = T -> hit
                    hits_T += 1
                    if o == 1:
                        fo.write(f"{dec_dir}\t   {result}\t      T\t\t    correct\n")
                    if counters[counter_to_acess] < 3:      #If previous value in the counter was WT [2]
                        counters[counter_to_acess] += 1 #Turn to ST [3]
                    else:
                        pass        #Already in ST [3]
                else:       #result = N -> miss
                    misses_T += 1
                    if o == 1:
                        fo.write(f"{dec_dir}\t   {result}\t      T\t\t    incorrect\n")
                    counters[counter_to_acess] -= 1     #Turns either from ST to WT or from WT to WN 
            cont += 1   #adds up to the line reader counter
            if o == 1 and cont == 5000:     #If debug mode is on:
                print("\n---------------------------------------------------------------")
                print("Prediction Parameters:\t\t\t\t\t")
                print("---------------------------------------------------------------")
                print(f"Branch Prediction type:\t\t\t\t     {mode}")
                print(f"BHT size (entries):\t\t\t\t\t{s}")
                print(f"Global history register size:\t\t\t\t{gh}")
                print(f"Private history register size:\t\t\t\t{ph}")
                print("---------------------------------------------------------------")
                print("Simulation Results:\t\t\t\t\t")
                print("---------------------------------------------------------------")
                print(f"Number of branch:\t\t\t\t\t{cont}")
                print(f"Number of correct prediction of taken branches:\t\t{hits_T}")
                print(f"Number of incorrect prediction of taken branches:\t{misses_T}")
                print(f"Correct prediction of not taken branches:\t\t{hits_N}")
                print(f"Incorrect prediction of not taken branches:\t\t{misses_N}")
                print(f"Percentage of correct predictions:\t\t\t{((hits_T+hits_N)/cont*100)}")
                print("---------------------------------------------------------------")
                print(f'Total time: {time.time()-starting_time}')
                break
    return mode, cont, hits_T, misses_T, hits_N, misses_N

def global_h(s, o, gh, ph, file_name):
    '''
    Global history branch predictor: this predictor uses a reg that saves the result of the branch (history) and an array of counters to store the different states.
    -Inputs:
            -s: BTH table size. In this function is used for sizing the counter array.
            -o: Debug flag wich prints locally the results and stops the iterations at 5k.
            -gh: Gshare reg size. 
            -ph: Pshare reg siz. In this function is used to print the value given by the user at the end.
            -file_name: name of the compressed file to be read in the function.

        -Outputs:
            -mode: String with the branch predictors name
            -cont: Counter with the number of line of the compressed file reads.
            -hits_T: Counter for the correct T branches
            -misses_T: Counter for the incorrect T branches
            -hits_N: Counter for the incorrect N branches
            -misses_N: Size of the array of counters
    '''

    mode = 'Gshared'
    name = "gshare.txt"     #Name used to create the file if o = 1
    hits_T = 0              #Counter for the correct T branches
    misses_T = 0            #Counter for the incorrect T branches
    hits_N = 0              #Counter for the correct N branches
    misses_N = 0            #Counter for the incorrect N branches
    counters_size = 2**s    #Size of the array of counters
    counters = []           #Array of counters
    mask_s = 2**s - 1       #Creating a mask for the size of the direction

    reg = 0                 #Reg used to store the history.  
    mask_reg = 2**gh - 1    #Creating a mask for the size of the history reg

    #Initializing array of counters in strongly not taken [0]
    for element in range(counters_size):
        counters.append(0)

    cont = 0    #Counter for the lines in the file

    if o == 1:                  #Debug flag
        fo= open(name, "w+")    #Creates the file in write mode
        fo.write("    PC\t\tOutcome\t  Prediction\tCorrect/Incorrect\n")    #prints in file
    # reading the file
    with gzip.open(file_name, "r") as f:
        for line in f:
            #Parsing the string to a desired value
            string = line[:12].decode('utf-8')      #Taking the last 12 elements of the line and removing it's "0b" initializer 
            string = string.strip('\n')             #Removing new line seq of the string
            splitted = string.split(" ")            #Spliting string by spaces
            dec_dir = splitted[0]   #PC (decimal_direction)
            dec_dir = int(dec_dir)  #Parsing from str to int the PC
            result = splitted[1]    #Result of the brach (T/N)
            
            #Apliying the mask (&) to the PC
            masked_PC = int(dec_dir) & mask_s
            
            #Apliying the mask (&) to the hist. reg
            masked_reg = reg & mask_reg

            #Prediction making:
            counter_to_acess = masked_reg ^ masked_PC    #Final counter to acess (XOR mask)

            #Counter to access handling the last s bits:
            counter_to_acess = counter_to_acess & mask_s

                        # Definition of the possible cases:
            if counters[counter_to_acess] == 0 or counters[counter_to_acess] == 1:
                #This case is for N prediction
                if result == 'N':   #result = N -> hit
                    reg = reg << 1  #Left displacement of a 1 in the reg to state the hit
                    hits_N += 1
                    if o == 1:
                        fo.write(f"{dec_dir}\t   {result}\t      N\t\t    correct\n")
                    if counters[counter_to_acess] > 0:  #If previous value  in the counter was WN [1]
                        counters[counter_to_acess] -= 1 #Turn to SN [0]
                    else:
                        counters[counter_to_acess] += 1 #If it was SN [0]
                else:       #result = T -> miss
                    reg = reg << 1  #Left displacement of a 1 in the reg to state the miss
                    reg += 1    #Making the miss turn to 0 in the hist. reg
                    misses_N += 1
                    if o == 1:
                        fo.write(f"{dec_dir}\t   {result}\t      N\t\t    incorrect\n")
                    counters[counter_to_acess] += 1     #Move either from SN to WN or from WN to WT

            elif counters[counter_to_acess] == 2 or counters[counter_to_acess] == 3:
                #This case is for T prediction
                if result == 'T':   #result = T -> hit
                    reg = reg << 1  #Left displacement of a 1 in the reg to state the hit
                    reg += 1        #To keep in the same state of the hist reg.
                    hits_T += 1
                    if o == 1:
                        fo.write(f"{dec_dir}\t   {result}\t      T\t\t    correct\n")
                    if counters[counter_to_acess] < 3:  #If previous value in the counter was WT [2]
                        counters[counter_to_acess] += 1 #Turn to ST [3]
                    else:
                        pass    #Already in ST [3]
                else:        #result = N -> miss
                    reg = reg << 1  #Left displacement of a 1 in the reg to state the miss
                    misses_T += 1
                    if o == 1:
                        fo.write(f"{dec_dir}\t   {result}\t      T\t\t    incorrect\n")
                    counters[counter_to_acess] -= 1 #Turns either from ST to WT or from WT to WN 

            cont += 1    #adds up to the line reader counter
            if o == 1 and cont == 5000:      #If debug mode is on:
                print("\n---------------------------------------------------------------")
                print("Prediction Parameters:\t\t\t\t\t")
                print("---------------------------------------------------------------")
                print("Branch Prediction type:\t\t\t\t     GShare")
                print(f"BHT size (entries):\t\t\t\t\t{s}")
                print(f"Global history register size:\t\t\t\t{gh}")
                print(f"Private history register size:\t\t\t\t{ph}")
                print("---------------------------------------------------------------")
                print("Simulation Results:\t\t\t\t\t")
                print("---------------------------------------------------------------")
                print(f"Number of branch:\t\t\t\t\t{cont}")
                print(f"Number of correct prediction of taken branches:\t\t{hits_T}")
                print(f"Number of incorrect prediction of taken branches:\t{misses_T}")
                print(f"Correct prediction of not taken branches:\t\t{hits_N}")
                print(f"Incorrect prediction of not taken branches:\t\t{misses_N}")
                print(f"Percentage of correct predictions:\t\t\t{((hits_T+hits_N)/cont*100)}")
                print("---------------------------------------------------------------")
                print(f'Total time: {time.time()-starting_time}')
                break
    return mode, cont, hits_T, misses_T, hits_N, misses_N

def private_h(s, o, gh, ph, file_name):
    '''
    Private history branch predictor: this predictor uses a Branch History Table (BHT) that saves the last branch results in order to 'train' some of it's counters to predict better according to a bigger structure
    -Inputs:
            -s: BTH table size. In this function is used for sizing the counter array.
            -o: Debug flag wich prints locally the results and stops the iterations at 5k.
            -gh: Gshare reg size. In this function is used to print the value given by the user at the end.
            -ph: Pshare reg siz. 
            -file_name: name of the compressed file to be read in the function.

        -Outputs:
            -mode: String with the branch predictors name
            -cont: Counter with the number of line of the compressed file reads.
            -hits_T: Counter for the correct T branches
            -misses_T: Counter for the incorrect T branches
            -hits_N: Counter for the incorrect N branches
            -misses_N: Size of the array of counters
    '''

    mode = 'Pshared'
    name = "pshare.txt"     #Name used to create the file if o = 1
    hits_T = 0              #Counter for the correct T branches
    misses_T = 0            #Counter for the incorrect T branches
    hits_N = 0              #Counter for the correct N branches
    misses_N = 0            #Counter for the incorrect N branches
    counters_size = 2**s    #Size of the array of counters
    counters = []           #Array of counters
    mask_s = 2**s - 1       #Creating a mask for the size of the direction
    reg = 0                 #Reg used to store the history.

    bht = []                #Table with the history of branches
    mask_ph = 2**ph - 1     #Creating a mask for the size of the hist table

    #Initializing array of counters and Branch Hist. Table in strongly not taken [0]
    for element in range(counters_size):
        counters.append(0)
        bht.append(0)

    cont = 0        #Counter for the lines in the file
    if o == 1:      #Debug flag
        fo= open(name, "w+")     #Creates the file in write mode
        fo.write("    PC\t\tOutcome\t  Prediction\tCorrect/Incorrect\n")    #prints in file

    # reading the file
    with gzip.open(file_name, "r") as f:
        for line in f:
            #Parsing the string to a desired value
            string = line[:12].decode('utf-8')      #Taking the last 12 elements of the line and removing it's "0b" initializer 
            string = string.strip('\n')             #Removing new line seq of the string
            splitted = string.split(" ")            #Spliting string by spaces
            dec_dir = splitted[0]   #PC (decimal_direction)
            dec_dir = int(dec_dir)  #Parsing from str to int the PC
            result = splitted[1]    #Result of the brach (T/N)
            
            #Apliying the mask (&) to the PC
            masked_PC = dec_dir & mask_s

            #Apliying the mask (&) to the value of the postion req position on the BHT
            reg = bht[masked_PC] & mask_ph

            #Prediction making:
            counter_to_acess = reg ^ masked_PC      #XOR mask for the value to acess in the counter
            counter_to_acess = counter_to_acess & mask_s   #Final counter to acess (taking the last s bits )

                        # Definition of the possible cases:
            if counters[counter_to_acess] == 0 or counters[counter_to_acess] == 1:
                #This case is for N prediction
                if result == 'N':       #result = N -> hit
                    bht[masked_PC] = bht[masked_PC] << 1 #Left displacement of a 1 in the value of the BHT table to state the hit
                    hits_N += 1

                    if counters[counter_to_acess] > 0:  #If previous value  in the counter was WN [1]
                        counters[counter_to_acess] -= 1 #Turn to SN [0]
                    if o == 1:
                        fo.write(f"{dec_dir}\t   {result}\t      N\t\t    correct\n")
                else:   
                    bht[masked_PC] = bht[masked_PC] << 1    #Left displacement of a 1 in the reg to state the miss
                    bht[masked_PC] += 1      #Making the miss turn to 0 in the BHT
                    counters[counter_to_acess] += 1 ##Move either from SN to WN or from WN to WT
                    misses_N += 1
                    if o == 1:
                        fo.write(f"{dec_dir}\t   {result}\t      N\t\t    incorrect\n")

            elif counters[counter_to_acess] == 2 or counters[counter_to_acess] == 3:
                #This case is for T prediction
                if result == 'T':   #result = T -> hit
                    bht[masked_PC] = bht[masked_PC] << 1    #Left displacement of a 1 in the reg to state the hit
                    bht[masked_PC] += 1 #Making the hit turn to 0 in the BHT
                    hits_T += 1
                    if counters[counter_to_acess] < 3:  #If previous value in the counter was WT [2]
                        counters[counter_to_acess] += 1 #Turn to ST [3]
                    if o == 1:
                        fo.write(f"{dec_dir}\t   {result}\t      T\t\t    correct\n")
                else:
                    bht[masked_PC] = bht[masked_PC] << 1    #Left displacement of a 1 in the reg to state the miss
                    counters[counter_to_acess] -= 1     #Making the hit turn to 1 in the BHT
                    misses_T += 1
                    if o == 1:
                        fo.write(f"{dec_dir}\t   {result}\t      T\t\t    incorrect\n")

            cont += 1   #adds up to the line reader counter
            if o == 1 and cont == 5000:   #If debug mode is on:
                print("\n---------------------------------------------------------------")
                print("Prediction Parameters:\t\t\t\t\t")
                print("---------------------------------------------------------------")
                print("Branch Prediction type:\t\t\t\t     PShare")
                print(f"BHT size (entries):\t\t\t\t\t{s}")
                print(f"Global history register size:\t\t\t\t{gh}")
                print(f"Private history register size:\t\t\t\t{ph}")
                print("---------------------------------------------------------------")
                print("Simulation Results:\t\t\t\t\t")
                print("---------------------------------------------------------------")
                print(f"Number of branch:\t\t\t\t\t{cont}")
                print(f"Number of correct prediction of taken branches:\t\t{hits_T}")
                print(f"Number of incorrect prediction of taken branches:\t{misses_T}")
                print(f"Correct prediction of not taken branches:\t\t{hits_N}")
                print(f"Incorrect prediction of not taken branches:\t\t{misses_N}")
                print(f"Percentage of correct predictions:\t\t\t{((hits_T+hits_N)/cont*100)}")
                print("---------------------------------------------------------------")
                print(f'Total time: {time.time()-starting_time}')
                break
    return mode, cont, hits_T, misses_T, hits_N, misses_N

def tournament(s, o, gh, ph, file_name):
    '''
    Tournament branch predictor: This predictor uses all of the prevoius structures to handle the branches possible results. 
    -Inputs:
            -s: BTH table size. In this function is used for sizing the counter array.
            -o: Debug flag wich prints locally the results and stops the iterations at 5k.
            -gh: Gshare reg size. 
            -ph: Pshare reg siz. 
            -file_name: name of the compressed file to be read in the function.

        -Outputs:
            -mode: String with the branch predictors name
            -cont: Counter with the number of line of the compressed file reads.
            -hits_T: Counter for the correct T branches
            -misses_T: Counter for the incorrect T branches
            -hits_N: Counter for the incorrect N branches
            -misses_N: Size of the array of counters
    '''
    mode = 'Tournament'
    name = "tournament.txt"     #Name used to create the file if o = 1
    hits_T = 0                  #Counter for the correct T branches
    misses_T = 0                #Counter for the incorrect T branches
    hits_N = 0                  #Counter for the correct N branches
    misses_N = 0                #Counter for the incorrect N branches
    counters_size = 2**s        #Size of the array of counters
    counters = []               #Array of counters
    mask_s = 2**s - 1           #Creating a mask for the size of the direction
    bht = []                    #Table with the history of branches
    mask_ph = 2**ph - 1         #Creating a mask for the size of the hist table       
    mask_gh = 2**gh - 1         #Creating a mask for the size of the history reg
    reg = 0                     #Reg used to store the history.   

    glob = []                   #Array for storing the branches made in the gshare stage         
    priv = []                   #Array for storing the branches made in the pshare stage              


    pred_g= ''                  #String to assign a T/N value for the gshare stage comparison in the Tournament stage             
    pred_p= ''                  ##String to assign a T/N value for the pshare stage comparison in the Tournament stage 
    pred_t= ''                  #String to compare with the likely values from gshare or pshare stages

    #Initializing array values in SN [0]
    for element in range(counters_size):
        counters.append(0)
        bht.append(0)
        glob.append(0)
        priv.append(0)

    cont = 0    #Counter for the lines in the file
    if o == 1:                  #Debug flag
        fo= open(name, "w+")    #Creates the file in write mode
        fo.write("    PC\t\tOutcome\t  Prediction\tCorrect/Incorrect\n")    #prints in file
    # reading the file
    with gzip.open(file_name, "r") as f:
        for line in f:
            #Parsing the string to a desired value
            string = line[:12].decode('utf-8')      #Taking the last 12 elements of the line and removing it's "0b" initializer 
            string = string.strip('\n')             #Removing new line seq of the string
            splitted = string.split(" ")            #Spliting string by spaces
            dec_dir = splitted[0]   #PC (decimal_direction)
            dec_dir = int(dec_dir)  #Parsing from str to int the PC
            result = splitted[1]    #Result of the brach (T/N)

            #Apliying the mask (&) to the PC
            masked_PC = dec_dir & mask_s

            #Re-masking the hist. reg as in it's respective function
            hist_g = reg & mask_gh
            hist_p = bht[masked_PC] & mask_ph

            #Re-masking the gshare stage as in it's respective function
            g_counter_to_acess =  hist_g ^ masked_PC
            g_counter_to_acess = g_counter_to_acess & mask_s

            #Re-masking the pshare stage as in it's respective function
            p_counter_to_acess = hist_p ^ masked_PC
            p_counter_to_acess = p_counter_to_acess & mask_s

        #g_share logic statement
            if glob[g_counter_to_acess] == 0 or glob[g_counter_to_acess] == 1:
                #This case is for N prediction
                pred_g = 'N'    #Assigning the N value to the prediction
                if result == 'N':   #result = N -> hit
                    reg = reg << 1  #Left displacement of a 1 in the reg to state the hit
                    if glob[g_counter_to_acess] > 0:    #If previous value  in the counter was WN [1]
                        glob[g_counter_to_acess] -= 1   #Turn to SN [0]
                else:   #result = T -> miss
                    reg = reg << 1  #Left displacement of a 1 in the reg to state the miss
                    reg += 1    #Making the miss turn to 0 in the hist. reg
                    glob[g_counter_to_acess] += 1   #Move either from SN to WN or from WN to WT

            if glob[g_counter_to_acess] == 2 or glob[g_counter_to_acess] == 3:
                #This case is for T prediction
                pred_g = 'T'    #Assigning the T value to the prediction
                if result == 'T':   #result = T -> hit
                    reg = reg << 1  #Left displacement of a 1 in the reg to state the hit
                    reg += 1    #To keep in the same state of the hist reg.
                    if glob[g_counter_to_acess] < 3:    #If previous value in the counter was WT [2]
                        glob[g_counter_to_acess] += 1   #Turn to ST [3]
                else:   #result = N -> miss
                    reg = reg << 1  #Left displacement of a 1 in the reg to state the miss
                    glob[g_counter_to_acess] -= 1   

        #p_share logic statement
            if priv[p_counter_to_acess] == 0 or priv[p_counter_to_acess] == 1:
                #This case is for N prediction
                pred_p = 'N'    #Assigning the N value to the prediction
                if result == 'N':   #result = N -> hit
                    bht[masked_PC] = bht[masked_PC] << 1     #Left displacement of a 1 in the reg to state the hit

                    if priv[p_counter_to_acess] > 0:     #If previous value  in the counter was WN [1]
                            priv[p_counter_to_acess] -= 1   #Turn to SN [0]
                    else:   #result = T -> miss
                        bht[masked_PC] = bht[masked_PC] << 1    #Left displacement of a 1 in the BHT to state the miss
                        bht[masked_PC] += 1 #Making the miss turn to 0 in the BHT
                        priv[p_counter_to_acess] += 1   #Move either from SN to WN or from WN to WT

            if priv[p_counter_to_acess] == 2 or priv[p_counter_to_acess] == 3:
                #This case is for T prediction
                pred_p = 'T'    #Assigning the N value to the prediction
                if result == 'T':    #result = N -> hit
                    bht[masked_PC] = bht[masked_PC] << 1    #Left displacement of a 1 in the reg to state the hit
                    bht[masked_PC] += 1     #Making the hit turn to 0 in the BHT
                    if priv[p_counter_to_acess] < 3:    #If previous value in the counter was WT [2]
                        priv[p_counter_to_acess] += 1   #Turn to ST [3]
                else:   #result = N -> miss
                    bht[masked_PC] = bht[masked_PC] << 1    #Left displacement of a 1 in the reg to state the miss
                    priv[p_counter_to_acess] -= 1   #Making the hit turn to 1 in the BHT

        #Tournament logic statement
            if counters[masked_PC] == 0 or counters[masked_PC] == 1:
                #Checks for the value in the array counter for N prediction
                pred_t = pred_p     #assigns the prediction of pshared to tournament
                if o == 1:
                    fo.write(f"{dec_dir}\t P\t\t {result}\t    ")

            if counters[masked_PC] == 2 or counters[masked_PC] == 3:
                #Checks for the value in the array counter for T prediction
                pred_t = pred_g     #assigns the prediction of gshared to tournament
                if o == 1:
                    fo.write(f"{dec_dir}\t G\t\t {result}\t    ")

            if pred_t == 'N':       #If the value assigned to Tournament pred is N
                if result == 'N':   #And the result of the branch makes a hit
                    hits_N += 1
                    if o == 1:
                        fo.write(f"N\t\t    correct\n") #Prints correct
                else:               #If the result of the branch makes a miss
                    misses_N += 1
                    if o == 1:
                        fo.write(f"N\t\t    incorrect\n")   #Prints incorrect
                    
            if pred_t == 'T':       #If the value assigned to Tournament pred is T
                if result == 'T':   #And the result of the branch makes a hit
                    hits_T += 1
                    if o == 1:
                        fo.write(f"T\t\t    correct\n") #Prints correct
                else:              #If the result of the branch makes a miss
                    misses_T += 1
                    if o == 1:
                        fo.write(f"T\t\t    incorrect\n")   #Prints incorrect

            if pred_g != pred_p:    #If the values on the gshared pred and pshared pred differ (miss):
                if pred_g == result:    #And the result of ghared is the result of the branch
                    if counters[masked_PC] < 3: 
                        counters[masked_PC] += 1    #Switch the counter array value to a new one
                else:   #And the result of phared is the result of the branch
                    if counters[masked_PC] > 0:
                        counters[masked_PC] -= 1    #And the result of ghared is the result of the branch

            cont += 1    #adds up to the line reader counter
            if o == 1 and cont == 5000: #If debug mode is on:
                print("\n---------------------------------------------------------------")
                print("Prediction Parameters:\t\t\t\t\t")
                print("---------------------------------------------------------------")
                print("Branch Prediction type:\t\t\t\t     Tournament")
                print(f"BHT size (entries):\t\t\t\t\t{s}")
                print(f"Global history register size:\t\t\t\t{gh}")
                print(f"Private history register size:\t\t\t\t{ph}")
                print("---------------------------------------------------------------")
                print("Simulation Results:\t\t\t\t\t")
                print("---------------------------------------------------------------")
                print(f"Number of branch:\t\t\t\t\t{cont}")
                print(f"Number of correct prediction of taken branches:\t\t{hits_T}")
                print(f"Number of incorrect prediction of taken branches:\t{misses_T}")
                print(f"Correct prediction of not taken branches:\t\t{hits_N}")
                print(f"Incorrect prediction of not taken branches:\t\t{misses_N}")
                print(f"Percentage of correct predictions:\t\t\t{((hits_T+hits_N)/cont*100)}")
                print("---------------------------------------------------------------")
                print(f'Total time: {time.time()-starting_time}')
                break
    return mode, cont, hits_T, misses_T, hits_N, misses_N

def main(argv):
    '''
        Main function with inputs: 
            -argv: arguments given by the user in the command line
        And outputs:
            -Printing to console the report of the processing
    '''
    s = 0   #Default values for the options given by the user
    bp= 0   #Default values for the options given by the user
    o= 0    #Default values for the options given by the user
    gh= 0   #Default values for the options given by the user
    ph = 0  #Default values for the options given by the user
    file_name = 'branch-trace-gcc.trace.gz' #Default value to the compressed file name

    el_num = 0  #Counter for the element of the options given by the user
        #Loop for assigning the variables to its values given by the user
    for i in range(len(argv)):
        el = argv[i]
        if el_num == len(argv)-1:  #LAST ELEMENT
            break
        else:
            try:
                next_el = argv[i+1]
            except IndexError:
                next_el = el
        if el == '-o':
            o = int(next_el)
        if el == '-s':
            s = int(next_el)
        if el == '-bp':
            bp = int(next_el)
        if el == '-gh':
            gh = int(next_el)
        if el == '-ph':
            ph = int(next_el)
        if el == '-f':
            file_name = next_el

    if bp == 0: #Calls the bimodal function
        mode, cont, hits_T, misses_T, hits_N, misses_N = bimodal(s, o, gh, ph, file_name)
    if bp == 1: #Calls the gshared function
        mode, cont, hits_T, misses_T, hits_N, misses_N = global_h(s, o, gh, ph, file_name)
    if bp == 2: #Calls the pshared function
        mode, cont, hits_T, misses_T, hits_N, misses_N = private_h(s, o, gh, ph, file_name)
    if bp == 3: #Calls the tournament function
        mode, cont, hits_T, misses_T, hits_N, misses_N = tournament(s, o, gh, ph, file_name)


    #Printing sequence of final values if debug mode is not selected
    if o == 0:
        print("\n---------------------------------------------------------------")
        print("Prediction Parameters:\t\t\t\t\t")
        print("---------------------------------------------------------------")
        print(f"Branch Prediction type:\t\t\t\t     {mode}")
        print(f"BHT size (entries):\t\t\t\t\t{s}")
        print(f"Global history register size:\t\t\t\t{gh}")
        print(f"Private history register size:\t\t\t\t{ph}")
        print("---------------------------------------------------------------")
        print("Simulation Results:\t\t\t\t\t")
        print("---------------------------------------------------------------")
        print(f"Number of branch:\t\t\t\t\t{cont}")
        print(f"Number of correct prediction of taken branches:\t\t{hits_T}")
        print(f"Number of incorrect prediction of taken branches:\t{misses_T}")
        print(f"Correct prediction of not taken branches:\t\t{hits_N}")
        print(f"Incorrect prediction of not taken branches:\t\t{misses_N}")
        print(f"Percentage of correct predictions:\t\t\t{((hits_T+hits_N)/cont*100)}")
        print("---------------------------------------------------------------")
        print(f'Total time: {time.time-starting_time}')

if __name__ == "__main__":
    main(sys.argv[1:])
