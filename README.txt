*Notice: Given that python can handle compressed files internally, with [-f] you can specify the compressed file to open otherwise it will 
look for the default name given.

USAGE: 
    -In the makefile provided there are the lines used in the debug stage of the code for easy revision of the results.
    -If you'd like to use your own options other than the ones given:

        python3 tarea.py -s [int] -o [int] -bp [int] -gh [int] -ph [int] -f [str]

        The default values for the integer options are 0
        The default value for the -f option is: branch-trace-gcc.trace.gz

        Note that if you want to use another file, and it is not in the same folder as this files, you must specify its entire path.


Emmanuel Rivel M. 